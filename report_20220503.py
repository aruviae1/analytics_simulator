import telegram
import matplotlib.pyplot as plt
import seaborn as sns
import pandahouse
import io

connection = {
    'host': 'https://clickhouse.lab.karpov.courses',
    'password': 'dpo_python_2020',
    'user': 'student',
    'database': 'simulator_20220420'#'simulator'
}

bot = telegram.Bot(token = '5246519782:AAH7ATCxFVxzCf_Zb8vtjRPALwziS77LA7Q')

chat_id = 361133268

#Запрос и данные для отчета
q = """ SELECT  toDate(time) DT,
                count(DISTINCT user_id) AS DAU,
                countIf(user_id, action='view') cnt_Views,
                countIf(user_id, action='like') cnt_Likes,
                round(countIf(user_id, action='like')/CountIf(user_id, action='view')*100,2) AS CTR
        FROM simulator_20220420.feed_actions 
        WHERE toDate(time) >= today()-7 and toDate(time) <> today()
        GROUP BY toDate(time)
    """
df = pandahouse.read_clickhouse(q, connection=connection)

#Формирование отчета
report_txt = 'Отчет за {0}: DAU: {1}, Просмотры: {2}, Лайки: {3}, CTR: {4}%'.format \
                (df['DT'].iloc[-1],
                 df['DAU'].iloc[-1],
                 df['cnt_Views'].iloc[-1], 
                 df['cnt_Likes'].iloc[-1],
                 df['CTR'].iloc[-1])
#Отправка отчета
bot.sendMessage(chat_id = chat_id, text = report_txt)

#Формирование графика
plt.style.use('ggplot')
fig, ax = plt.subplots(2, 2, sharex=True, figsize=[10, 7])

plt.suptitle('Динамика KPI за предыдущую неделю', fontsize=14, fontweight='bold')

plt.subplot(2, 2, 1)
sns.lineplot(data = df, x='DT', y='DAU')
plt.title('DAU')

plt.subplot(2, 2, 2)
sns.lineplot(data = df, x='DT', y='CTR')
plt.title('CTR, %')

plt.subplot(2, 2, 3)
sns.lineplot(data = df, x='DT', y='cnt_Views')
plt.title('Просмотры')

plt.subplot(2, 2, 4)
sns.lineplot(data = df, x='DT', y='cnt_Likes')
plt.title('Лайки')

fig.autofmt_xdate()
plt.tight_layout()

plot_object = io.BytesIO()
plt.savefig(plot_object)
plot_object.seek(0)
plot_object.name = 'report_plot.png'
plt.close()

#Отправка графика
bot.sendPhoto(chat_id=chat_id, photo=plot_object)
